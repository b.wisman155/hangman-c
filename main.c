#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


char word[20];
char guess[20];
int guessed = 0;


int draw_board(void) {
    system("clear");
    printf("Welcome to hangman\n\n");
    int i;
    for (i = 0; i < strlen(word); i++) {
        if (guess[i] != NULL) {
            printf("%c ", guess[i]);
        } else {
            printf("_ ");
        }
    }

    while (guessed != 1) {
        char letter[1];
        printf("\n\nGuess letter: ");
        scanf(" %c", &letter);
        for (i = 0; i < strlen(word); i++) {
            if (word[i] == letter[0]) {
                guess[i] = word[i];
            }
        }
        if (strncmp(guess, word, strlen(word)) == 0) {
            printf("YOU GUESSED IT!!!!\n");
            exit(0);
        }
        draw_board();
    }
}

int check_char(char c) {
    if (isspace(c)) {
        return 0;
    }
    if (isalpha(c)) {
        return 1;
    }
    return 0;
}

int main(void) {
    int allowed = 0;
    int i;

    printf("Welcome to hangman\n\n");
    while (allowed == 0) {
        printf("Word to guess: ");
        scanf(" %[^\n]s", word);
        for (i = 0; i < strlen(word); i++) {
            allowed = check_char(word[i]);
            if (allowed == 0) {
                printf("contains invalid chars\n");
                break;
            }
        }
    }
    draw_board();
    return 0;
}

